'use strict';
function dropDown() {
    var showMenu = document.getElementById('drop');
    var burgerNav = document.getElementById('burger');
    if (showMenu != null ) {
        if (showMenu.style.display=='none' ) {
            showMenu.style.display='block';
            burgerNav.className="red-burger";
        } else {
            showMenu.style.display='none';
            burgerNav.className="head-menu";
        }
    }
}

function dropDownRed() {
    var showOne = document.getElementById('first-menu');
    if (showOne != null ) {
        if (showOne.style.display=='none' ) {
            showOne.style.display='block';
        } else {
            showOne.style.display='none';
        }
    }
}
function dropDownRedAnother() {
    var showTwo = document.getElementById('second-menu');
    var arrowRight = document.getElementsByClassName('arrow-drop2');
    if (showTwo != null ) {
        if (showTwo.style.display=='none' ) {
            showTwo.style.display='block';
        } else {
            showTwo.style.display='none';
        }
    }
}