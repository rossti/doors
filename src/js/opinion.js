function showFormOpinion() {
    var showScreen = document.getElementById('opinionBackScreen');
    if (showScreen != null ) {
        showScreen.style.display='block';
    }
    var showForm = document.getElementById('opinionBackForm');
    if (showForm != null) {
        showForm.style.display='block';
    }
}

function hideOpinionScreen() {
    var showScreen = document.getElementById('opinionBackScreen');
    if (showScreen != null ) {
        showScreen.style.display='none';
    }
    var showForm = document.getElementById('opinionBackForm');
    if (showForm != null) {
        showForm.style.display='none';
    }
}
